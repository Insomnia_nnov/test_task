package com.ru.android.sbelov.bsc_test_task.model;


import android.os.Parcel;
import android.os.Parcelable;

import java.math.BigDecimal;


public class Account implements Parcelable {
    private int currencyId;
    private BigDecimal value;

    public Account(int currencyId, BigDecimal value) {
        this.currencyId = currencyId;
        this.value = value;
    }

    protected Account(Parcel in) {
        this.currencyId = in.readInt();
        this.value = new BigDecimal(in.readString());
    }

    public static final Creator<Account> CREATOR = new Creator<Account>() {
        @Override
        public Account createFromParcel(Parcel in) {
            return new Account(in);
        }

        @Override
        public Account[] newArray(int size) {
            return new Account[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.currencyId);
        dest.writeString(this.value.toString());
    }

    public int getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(int currencyId) {
        this.currencyId = currencyId;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }
}
