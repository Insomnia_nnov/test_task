package com.ru.android.sbelov.bsc_test_task.database.money;

public class Currency {
    private int id;
    private String code;
    private String name;

    public Currency(int id, String code, String name) {
        this.id = id;
        this.code = code;
        this.name = name;
    }

    public Currency(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
