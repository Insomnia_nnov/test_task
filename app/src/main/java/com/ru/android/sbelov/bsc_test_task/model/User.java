package com.ru.android.sbelov.bsc_test_task.model;


import java.util.List;


public class User {
    private String name;
    private List<Account> accountList;

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Account> getAccountList() {
        return accountList;
    }

    public void setAccountList(List<Account> accountList) {
        this.accountList = accountList;
    }
}
