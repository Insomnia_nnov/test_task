package com.ru.android.sbelov.bsc_test_task.adapters;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ru.android.sbelov.bsc_test_task.R;
import com.ru.android.sbelov.bsc_test_task.model.Account;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;


public class AccountsAdapter extends RecyclerView.Adapter<AccountsAdapter.AccountsViewHolder> {
    public static final String TAG = AccountsAdapter.class.getSimpleName();
    public static final int VIEW_TYPE_LEFT = 1;
    public static final int VIEW_TYPE_RIGHT = 2;

    private List<Account> mAccountsList = new ArrayList<>();

    public void setItems(Collection<Account> accounts) {
        mAccountsList.addAll(accounts);
        Collections.sort(mAccountsList, (o1, o2) -> (Integer.compare(o1.getCurrencyId(), o2.getCurrencyId())));
        notifyDataSetChanged();
    }

    public void clearItems() {
        mAccountsList.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        switch (mAccountsList.get(position).getCurrencyId()) {
            // Из-за некоторого хардкода, хардкор требуется и тут:
            // "RUR" -- 1
            // "USD" -- 2
            // "EUR" -- 3

            case 1:
                return VIEW_TYPE_LEFT;

            case 2:
            case 3:
                return VIEW_TYPE_RIGHT;

            default:
                return VIEW_TYPE_RIGHT;
        }
    }

    @NonNull
    @Override
    public AccountsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        switch (viewType) {
            case VIEW_TYPE_LEFT:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.left_orientation_cell, parent, false);
                break;

            case VIEW_TYPE_RIGHT:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.right_orientation_cell, parent, false);
                break;
            default:
                // todo add default xml layout for default cell; NPE may come here;
                break;
        }

        return new AccountsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AccountsViewHolder holder, int position) {
        holder.bind(mAccountsList.get(position));
    }

    @Override
    public int getItemCount() {
        return mAccountsList.size();
    }

    class AccountsViewHolder extends RecyclerView.ViewHolder {

        private ImageView mImageView;
        private TextView mNameView;

        AccountsViewHolder(View itemView) {
            super(itemView);

            mImageView = itemView.findViewById(R.id.image);
            mNameView = itemView.findViewById(R.id.text);
        }

        void bind(Account account) {
            // Из-за некоторого хардкода, хардкор требуется и тут:
            // "RUR" -- 1
            // "USD" -- 2
            // "EUR" -- 3
            int type = account.getCurrencyId();

            switch (type) {
                case 1:
                    mImageView.setImageResource(R.drawable.ruble);
                    break;

                case 2:
                    mImageView.setImageResource(R.drawable.dollar);
                    break;

                case 3:
                    mImageView.setImageResource(R.drawable.euro);
                    break;
                default:
                    break;
            }

            mNameView.setText(String.format("%s", account.getValue()));
        }
    }
}
