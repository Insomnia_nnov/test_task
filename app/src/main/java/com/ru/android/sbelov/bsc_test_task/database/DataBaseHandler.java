package com.ru.android.sbelov.bsc_test_task.database;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ru.android.sbelov.bsc_test_task.database.money.Currency;


public class DataBaseHandler extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "data.db";

    public DataBaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Data.MONEY.CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(Data.MONEY.DELETE);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public void addCurrency(Currency currency) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Data.MONEY.COLUMNS.COLUMN_CODE, currency.getCode());
        values.put(Data.MONEY.COLUMNS.COLUMN_NAME, currency.getName());

        db.insert(Data.MONEY.TABLE_NAME, null, values);
        db.close();
    }

    public Currency getCurrency(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(Data.MONEY.TABLE_NAME, new String[]
                { Data.MONEY.COLUMNS.COLUMN_ID,
                        Data.MONEY.COLUMNS.COLUMN_CODE,
                        Data.MONEY.COLUMNS.COLUMN_NAME},
                Data.MONEY.COLUMNS.COLUMN_ID + "=?", new String[] { String.valueOf(id)},
                null,
                null,
                null,
                null);

        Currency currency = null;

        if (null != cursor) {
            cursor.moveToFirst();
            currency = new Currency(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Data.MONEY.COLUMNS.COLUMN_ID))),
                    cursor.getString(cursor.getColumnIndex(Data.MONEY.COLUMNS.COLUMN_CODE)),
                    cursor.getString(cursor.getColumnIndex(Data.MONEY.COLUMNS.COLUMN_NAME)));
            cursor.close();
        }

        return currency;
    }

    public void deleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Data.MONEY.TABLE_NAME, null, null);
        db.close();
    }
}
