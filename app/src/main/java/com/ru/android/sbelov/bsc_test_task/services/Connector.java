package com.ru.android.sbelov.bsc_test_task.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.util.Log;


import com.ru.android.sbelov.bsc_test_task.activities.MainActivity;
import com.ru.android.sbelov.bsc_test_task.model.Account;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.math.BigDecimal;


public class Connector extends IntentService {

    public static final String TAG = Connector.class.getSimpleName();
    public static final String KEY_NAME = "_name";

    public static final String PARAM_STATUS = "_status";
    public static final String PARAM_DATA_ACCOUNTS = "_data_accounts";
    public static final int STATUS_OK = 0;
    public static final int STATUS_NOT_OK = -1;


    public static final int FAKE_SLEEP_DURATION = 3;


    public Connector() {
        super("Connector");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate()");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Log.d(TAG, "onHandleIntent()");
        String name = intent != null ? intent.getStringExtra(KEY_NAME) : null;
        Intent intentOut = new Intent(MainActivity.BROADCAST_ACTION);

        // fake connection:
        if (null != name) {
            try {
                TimeUnit.SECONDS.sleep(FAKE_SLEEP_DURATION);

                List<Account> accounts = new ArrayList<>();
                accounts.add(new Account(1, new BigDecimal(32000)));
                accounts.add(new Account(2, new BigDecimal(320000)));
                accounts.add(new Account(3, new BigDecimal(3200000)));

                accounts.add(new Account(1, new BigDecimal(64000)));
                accounts.add(new Account(2, new BigDecimal(6400000)));
                accounts.add(new Account(3, new BigDecimal(64000000)));

                accounts.add(new Account(1, new BigDecimal(128000)));
                accounts.add(new Account(2, new BigDecimal(1280000)));
                accounts.add(new Account(3, new BigDecimal(12800000)));

                intentOut.putParcelableArrayListExtra(Connector.PARAM_DATA_ACCOUNTS, (ArrayList<? extends Parcelable>) accounts);

                intentOut.putExtra(Connector.PARAM_STATUS, Connector.STATUS_OK);
                sendBroadcast(intentOut);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else {
            intentOut.putExtra(Connector.PARAM_STATUS, Connector.STATUS_NOT_OK); // ENOUGH for test_task
            sendBroadcast(intentOut);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }
}
