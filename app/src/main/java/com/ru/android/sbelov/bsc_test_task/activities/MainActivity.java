package com.ru.android.sbelov.bsc_test_task.activities;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.ru.android.sbelov.bsc_test_task.R;
import com.ru.android.sbelov.bsc_test_task.adapters.AccountsAdapter;
import com.ru.android.sbelov.bsc_test_task.database.DataBaseHandler;
import com.ru.android.sbelov.bsc_test_task.database.money.Currency;
import com.ru.android.sbelov.bsc_test_task.model.Account;
import com.ru.android.sbelov.bsc_test_task.model.User;
import com.ru.android.sbelov.bsc_test_task.services.Connector;


public class MainActivity extends Activity {
    public static final String TAG = MainActivity.class.getSimpleName();
    public static final String BROADCAST_ACTION = "com.ru.android.sbelov.bsc_test_task";

    private RecyclerView mAccountsList = null;
    private DataBaseHandler mDataBaseHandler = null;
    private User mUser = null;

    private CustomBroadcastReceiver mBroadcastReceiver = null;

    private void initViews() {
        mAccountsList = findViewById(R.id.rv_data);
        mAccountsList.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();

        mDataBaseHandler = new DataBaseHandler(this);

        // START :: HARDCODE
        mDataBaseHandler.deleteAll();
        mDataBaseHandler.addCurrency(new Currency("RUR", "RUBLE"));
        mDataBaseHandler.addCurrency(new Currency("USD", "DOLLAR"));
        mDataBaseHandler.addCurrency(new Currency("EUR", "EURO"));

        mUser = new User("Kodzima");
        // END :: HARDCODE

        mBroadcastReceiver = new CustomBroadcastReceiver();

        IntentFilter intentFilter = new IntentFilter(BROADCAST_ACTION);
        registerReceiver(mBroadcastReceiver, intentFilter);

        mAccountsList.setAdapter(new AccountsAdapter());

        Intent intent = new Intent(this, Connector.class).putExtra(Connector.KEY_NAME, "");
        startService(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mBroadcastReceiver);
    }

    private class CustomBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            int status = intent.getIntExtra(Connector.PARAM_STATUS, -1);

            switch (status) {
                case Connector.STATUS_OK:
                    Toast.makeText(context, R.string.toast_ok, Toast.LENGTH_LONG).show();
                    mUser.setAccountList(intent.<Account>getParcelableArrayListExtra(Connector.PARAM_DATA_ACCOUNTS));
                    ((AccountsAdapter)mAccountsList.getAdapter()).setItems(mUser.getAccountList());
                    mAccountsList.getAdapter().notifyDataSetChanged();
                    break;

                case Connector.STATUS_NOT_OK:
                    Toast.makeText(context, R.string.toast_not_ok, Toast.LENGTH_LONG).show();
                    break;

                default:
                    break;
            }
        }
    }
}
