package com.ru.android.sbelov.bsc_test_task.database;

public interface Data {
    interface MONEY {
        String TABLE_NAME = "money";

        interface COLUMNS {
            String COLUMN_ID = "_id";
            String COLUMN_CODE = "_code";
            String COLUMN_NAME = "_name";
        }

        String CREATE = "CREATE TABLE " + TABLE_NAME + " (" +
                COLUMNS.COLUMN_ID + " INTEGER PRIMARY KEY," +
                COLUMNS.COLUMN_CODE + " TEXT," +
                COLUMNS.COLUMN_NAME + " TEXT)";

        String DELETE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }
}
